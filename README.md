# i3onwindows

Run the i3 window manager using the Windows subsystem for Linux. 

I first followed the instructions here, except I installed Debian instead of Ubuntu: https://seanthegeek.net/234/graphical-linux-applications-bash-ubuntu-windows/

I then placed these files in Documents/Scripts and created shortcuts to i3.bat on my desktop and in the Windows Startup folder. The startup folder can be accessed by typing `shell:startup` in the run box (Winkey+R). I included an i3 icon to use with the shortcuts.
